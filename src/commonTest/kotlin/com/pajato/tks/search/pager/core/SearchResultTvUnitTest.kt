package com.pajato.tks.search.pager.core

import com.pajato.test.ReportingTestProfiler
import com.pajato.tks.common.core.jsonFormat
import kotlinx.serialization.encodeToString
import kotlin.test.Test
import kotlin.test.assertEquals

class SearchResultTvUnitTest : ReportingTestProfiler() {
    @Test fun `When creating a default search result tv object, verify behavior`() {
        val result = SearchResultTv()
        assertEquals(false, result.adult)
        assertEquals(-1, result.id)
        assertEquals("", result.name)
        assertEquals("", result.originalName)
        assertEquals(0.0, result.popularity)
        assertEquals("", result.posterPath)
        assertEquals(0.0, result.voteAverage)
        assertEquals(0, result.voteCount)
    }

    @Test fun `When serializing and deserializing a default search result tv object, verify behavior`() {
        val json = jsonFormat.encodeToString(SearchResultTv())
        assertEquals(SearchResultTv(), jsonFormat.decodeFromString(json))
    }
}
