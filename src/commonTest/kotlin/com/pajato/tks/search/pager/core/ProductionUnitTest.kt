package com.pajato.tks.search.pager.core

import com.pajato.test.ReportingTestProfiler
import com.pajato.tks.common.core.jsonFormat
import kotlinx.serialization.encodeToString
import kotlin.test.Test
import kotlin.test.assertEquals

class ProductionUnitTest : ReportingTestProfiler() {
    @Test fun `When creating a default production object, verify behavior`() {
        val production = Production()
        assertEquals(0, production.id)
    }

    @Test fun `When a test production object is serialized and deserialized, verify behavior`() {
        val production = Production()
        val json = jsonFormat.encodeToString(production)
        assertEquals("{}", json)
        assertEquals(0, jsonFormat.decodeFromString<Production>(json).id)
    }
}
