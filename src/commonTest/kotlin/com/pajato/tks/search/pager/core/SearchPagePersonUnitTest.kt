package com.pajato.tks.search.pager.core

import com.pajato.test.ReportingTestProfiler
import com.pajato.tks.common.core.jsonFormat
import kotlinx.serialization.encodeToString
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.test.fail

class SearchPagePersonUnitTest : ReportingTestProfiler() {
    @Test fun `When creating a default search page person response, verify behavior`() {
        val response = SearchPage<SearchResultPerson>()
        assertEquals(0, response.page)
        assertEquals(0, response.totalPages)
        assertEquals(0, response.totalResults)
        assertEquals(0, response.results.size)
    }

    @Test fun `When serializing a valid json search page person response, verify result`() {
        val loader = javaClass.classLoader
        val json: String = loader.getResource("abc-person.json")?.readText() ?: fail("Json file is invalid!")
        val searchResult: SearchPage<SearchResultPerson> = jsonFormat.decodeFromString(json)
        assertEquals(1, searchResult.page)
        assertEquals(4, searchResult.totalPages)
        assertEquals(78, searchResult.totalResults)
        assertEquals(1, searchResult.results.size)
        assertTrue(jsonFormat.encodeToString(searchResult).isNotEmpty())
    }
}
