package com.pajato.tks.search.pager.core

import com.pajato.tks.common.core.BACKDROP_PATH
import com.pajato.tks.common.core.GENRE_IDS
import com.pajato.tks.common.core.MEDIA_TYPE
import com.pajato.tks.common.core.ORIGINAL_LANGUAGE
import com.pajato.tks.common.core.ORIGINAL_TITLE
import com.pajato.tks.common.core.POSTER_PATH
import com.pajato.tks.common.core.RELEASE_DATE
import com.pajato.tks.common.core.VOTE_AVERAGE
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable public data class Production(
    val adult: Boolean = false,
    @SerialName(BACKDROP_PATH)
    val backdropPath: String = "",
    @SerialName(GENRE_IDS)
    val genreIds: List<Int> = listOf(),
    val id: Int = 0,
    @SerialName(MEDIA_TYPE)
    val mediaType: String = "",
    @SerialName(ORIGINAL_LANGUAGE)
    val originalLanguage: String = "",
    @SerialName(ORIGINAL_TITLE)
    val originalTitle: String = "",
    val overview: String = "",
    val popularity: Double = 0.0,
    @SerialName(POSTER_PATH)
    val posterPath: String = "",
    @SerialName(RELEASE_DATE)
    val releaseDate: String = "",
    val title: String = "",
    val video: Boolean = false,
    @SerialName(VOTE_AVERAGE)
    val voteAverage: Double = 0.0,
    val voteCount: Int = 0,
)
