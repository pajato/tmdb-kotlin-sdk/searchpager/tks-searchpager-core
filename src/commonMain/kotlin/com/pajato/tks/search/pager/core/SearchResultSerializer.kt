package com.pajato.tks.search.pager.core

import kotlinx.serialization.DeserializationStrategy
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.jsonObject

public object SearchResultSerializer : JsonContentPolymorphicSerializer<SearchResult>(SearchResult::class) {
    private val movieExclusiveKeys = listOf("title")
    private val personExclusiveKeys = listOf("gender")

    override fun selectDeserializer(element: JsonElement): DeserializationStrategy<SearchResult> = when {
        personExclusiveKeys.any { it in element.jsonObject } -> SearchResultPerson.serializer()
        movieExclusiveKeys.any { it in element.jsonObject } -> SearchResultMovie.serializer()
        else -> SearchResultTv.serializer()
    }
}
