package com.pajato.tks

import com.pajato.tks.common.core.jsonFormat
import com.pajato.tks.search.pager.core.SearchResult
import com.pajato.tks.search.pager.core.SearchResultMovie
import com.pajato.tks.search.pager.core.SearchResultPerson
import com.pajato.tks.search.pager.core.SearchResultTv
import kotlinx.serialization.json.Json
import kotlinx.serialization.modules.SerializersModule
import kotlinx.serialization.modules.polymorphic
import kotlinx.serialization.modules.subclass

private val module = SerializersModule {
    polymorphic(SearchResult::class) {
        subclass(SearchResultMovie::class)
        subclass(SearchResultPerson::class)
        subclass(SearchResultTv::class)
    }
}

internal val jsonSearchFormat = Json(from = jsonFormat) { serializersModule = module }
