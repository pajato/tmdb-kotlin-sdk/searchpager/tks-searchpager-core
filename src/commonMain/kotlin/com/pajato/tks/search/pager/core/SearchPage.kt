package com.pajato.tks.search.pager.core

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable public data class SearchPage<T : SearchResult>(
    val page: Int = 0,
    val results: List<T> = listOf(),
    @SerialName("total_pages")
    val totalPages: Int = 0,
    @SerialName("total_results")
    val totalResults: Int = 0,
)
