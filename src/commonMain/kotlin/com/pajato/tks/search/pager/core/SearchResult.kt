package com.pajato.tks.search.pager.core

import kotlinx.serialization.Serializable

@Serializable public sealed class SearchResult
