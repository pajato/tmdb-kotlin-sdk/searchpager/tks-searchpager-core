package com.pajato.tks.search.pager.core

import com.pajato.tks.common.core.TmdbId
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable public data class SearchResultPerson(
    val adult: Boolean = false,
    val gender: Int = 0,
    val id: TmdbId = -1,
    @SerialName("known_for")
    val knownFor: List<Production> = listOf(),
    @SerialName("known_for_department")
    val knownForDepartment: String = "",
    val name: String = "",
    @SerialName("original_name")
    val originalName: String = "",
    val popularity: Double = 0.0,
    @SerialName("poster_path")
    val posterPath: String = "",
    @SerialName("profile_path")
    val profilePath: String = "",
    @SerialName("vote_average")
    val voteAverage: Double = 0.0,
    @SerialName("vote_count")
    val voteCount: Int = 0,
) : SearchResult()
