package com.pajato.tks.search.pager.core

public interface PagerState<T> {
    public val isLoading: Boolean
    public val items: List<T>
    public val error: String
    public val endReached: Boolean
    public val page: Int
    public val totalItems: Int
}
